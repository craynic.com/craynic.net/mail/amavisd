#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$AMAVISD_CLAMAV_SERVERS" ]]; then
  echo "ClamAV servers not specified, cannot start"
  exit 1
fi

CFG_FILE="/etc/stunnel/stunnel-clamav.conf"
TEMPLATE_FILE="$CFG_FILE.template"

# shellcheck disable=SC2016
envsubst '${AMAVISD_CLAMAV_SERVERS}' < "$TEMPLATE_FILE" > "$CFG_FILE"
