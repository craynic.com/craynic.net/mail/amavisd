#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -z "$AMAVISD_HOSTNAME" ]]; then
  echo "Please specify a hostname." >/dev/stderr
  exit 1
fi

CFG_FILE="/etc/amavisd.conf"

sed -i "/^#\?\$myhostname[[:space:]]*=/c\\\$myhostname = '$AMAVISD_HOSTNAME';" -- "$CFG_FILE"
