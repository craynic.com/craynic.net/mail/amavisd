#!/usr/bin/env bash

set -Eeuo pipefail

AMAVIS_HOME="/var/amavis"
USER="amavis"
GROUP="amavis"

# create needed folder structure
# https://git.alpinelinux.org/aports/tree/main/amavis/APKBUILD
for DIR in tmp var db home quarantine; do
  install -dm750 -o "$USER" -g "$GROUP" "$AMAVIS_HOME/$DIR"
done

# fix permissions
chmod -R u+rwX,g+rX,o-rwx "$AMAVIS_HOME"
