use strict;

#$myhostname = undef;
$mydomain = "$myhostname";
$max_servers = 10;
$daemon_user = 'amavis';
$daemon_group = 'amavis';

$MYHOME = '/var/amavis';
$TEMPBASE = "$MYHOME/tmp";
$ENV{TMPDIR} = $TEMPBASE;
$QUARANTINEDIR = "$MYHOME/quarantine";

$log_level = 3;
$log_recip_templ = undef;
$do_syslog = 0;

$enable_db = 0;
$nanny_details_level = 2;
$enable_dkim_verification = 1;
$enable_dkim_signing = 0;

@local_domains_maps = ();
@mynetworks = qw();

$unix_socketname = "$MYHOME/amavisd.sock";

$policy_bank{'ORIGINATING'} = {
  originating => 1,
  allow_disclaimers => 1,
  warnbadhsender   => 1,
  smtpd_discard_ehlo_keywords => [],
  bypass_banned_checks_maps => [1],
  terminate_dsn_on_notify_success => 0,
};

$interface_policy{'SOCK'} = 'AM.PDP-SOCK';
$policy_bank{'AM.PDP-SOCK'} = {
  protocol => 'AM.PDP',
  auth_required_release => 0,
};

$sa_tag_level_deflt  = -100;
$sa_tag2_level_deflt = 6.2;
$sa_kill_level_deflt = 6.9;
$sa_dsn_cutoff_level = 10;
$sa_crediblefrom_dsn_cutoff_level = 18;
$sa_quarantine_cutoff_level = 25;
$penpals_bonus_score = 8;
$penpals_threshold_high = $sa_kill_level_deflt;
$bounce_killer_score = 100;

$sa_mail_body_size_limit = 400*1024;
$sa_local_tests_only = 0;

$mailfrom_to_quarantine = '';

@addr_extension_virus_maps      = ('virus');
@addr_extension_banned_maps     = ('banned');
@addr_extension_spam_maps       = ('spam');
@addr_extension_bad_header_maps = ('badh');

$path = '/usr/local/sbin:/usr/local/bin:/usr/sbin:/sbin:/usr/bin:/bin';

$MAXLEVELS = 14;
$MAXFILES = 3000;
$MIN_EXPANSION_QUOTA =      100*1024;
$MAX_EXPANSION_QUOTA = 500*1024*1024;

$sa_spam_subject_tag = '***Spam*** ';
$defang_virus  = 1;  # MIME-wrap passed infected mail
$defang_banned = 1;  # MIME-wrap passed mail containing banned name
$defang_by_ccat{CC_BADH.",3"} = 1;  # NUL or CR character in header
$defang_by_ccat{CC_BADH.",5"} = 1;  # header line longer than 998 characters
$defang_by_ccat{CC_BADH.",6"} = 1;  # header field syntax error

$notify_method  = undef;
$forward_method = undef;

$final_virus_destiny = D_DISCARD;
$final_banned_destiny = D_DISCARD;
$final_spam_destiny = D_PASS;
$final_spam_destiny = D_DISCARD;
$final_bad_header_destiny = D_PASS;
$bad_header_quarantine_method = undef;

# REMAINING IMPORTANT VARIABLES ARE LISTED HERE BECAUSE OF LONGER ASSIGNMENTS

@keep_decoded_original_maps = (new_RE(
  qr'^MAIL$',                # let virus scanner see full original message
  qr'^MAIL-UNDECIPHERABLE$', # same as ^MAIL$ if mail is undecipherable
  qr'^(ASCII(?! cpio)|text|uuencoded|xxencoded|binhex)'i,
# qr'^Zip archive data',     # don't trust Archive::Zip
));


$banned_filename_re = new_RE(

### BLOCKED ANYWHERE
# qr'^UNDECIPHERABLE$',  # is or contains any undecipherable components
  qr'^\.(exe-ms|dll)$',                   # banned file(1) types, rudimentary
# qr'^\.(exe|lha|cab|dll)$',              # banned file(1) types

### BLOCK THE FOLLOWING, EXCEPT WITHIN UNIX ARCHIVES:
# [ qr'^\.(gz|bz2)$'             => 0 ],  # allow any in gzip or bzip2
  [ qr'^\.(rpm|cpio|tar)$'       => 0 ],  # allow any in Unix-type archives

  qr'.\.(pif|scr)$'i,                     # banned extensions - rudimentary
# qr'^\.zip$',                            # block zip type

### BLOCK THE FOLLOWING, EXCEPT WITHIN ARCHIVES:
# [ qr'^\.(zip|rar|arc|arj|zoo)$'=> 0 ],  # allow any within these archives

  qr'^application/x-msdownload$'i,        # block these MIME types
  qr'^application/x-msdos-program$'i,
  qr'^application/hta$'i,

# qr'^message/partial$'i,         # rfc2046 MIME type
# qr'^message/external-body$'i,   # rfc2046 MIME type

# qr'^(application/x-msmetafile|image/x-wmf)$'i,  # Windows Metafile MIME type
# qr'^\.wmf$',                            # Windows Metafile file(1) type

  # block certain double extensions in filenames
  qr'^(?!cid:).*\.[^./]*[A-Za-z][^./]*\.\s*(exe|vbs|pif|scr|bat|cmd|com|cpl|dll)[.\s]*$'i,

# qr'\{[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}\}?'i, # Class ID CLSID, strict
# qr'\{[0-9a-z]{4,}(-[0-9a-z]{4,}){0,7}\}?'i, # Class ID extension CLSID, loose

  qr'.\.(exe|vbs|pif|scr|cpl)$'i,             # banned extension - basic
# qr'.\.(exe|vbs|pif|scr|cpl|bat|cmd|com)$'i, # banned extension - basic+cmd
# qr'.\.(ade|adp|app|bas|bat|chm|cmd|com|cpl|crt|emf|exe|fxp|grp|hlp|hta|
#        inf|ini|ins|isp|js|jse|lib|lnk|mda|mdb|mde|mdt|mdw|mdz|msc|msi|
#        msp|mst|ocx|ops|pcd|pif|prg|reg|scr|sct|shb|shs|sys|vb|vbe|vbs|vxd|
#        wmf|wsc|wsf|wsh)$'ix,                # banned extensions - long
# qr'.\.(asd|asf|asx|url|vcs|wmd|wmz)$'i,     # consider also
# qr'.\.(ani|cur|ico)$'i,                 # banned cursors and icons filename
# qr'^\.ani$',                            # banned animated cursor file(1) type
# qr'.\.(mim|b64|bhx|hqx|xxe|uu|uue)$'i,  # banned extension - WinZip vulnerab.
);


# ENVELOPE SENDER SOFT-WHITELISTING / SOFT-BLACKLISTING

@score_sender_maps = ({ # a by-recipient hash lookup table,
                        # results from all matching recipient tables are summed

  ## site-wide opinions about senders (the '.' matches any recipient)
  '.' => [  # the _first_ matching sender determines the score boost

   new_RE(  # regexp-type lookup table, just happens to be all soft-blacklist
    [qr'^(bulkmail|offers|cheapbenefits|earnmoney|foryou)@'i         => 5.0],
    [qr'^(greatcasino|investments|lose_weight_today|market\.alert)@'i=> 5.0],
    [qr'^(money2you|MyGreenCard|new\.tld\.registry|opt-out|opt-in)@'i=> 5.0],
    [qr'^(optin|saveonlsmoking2002k|specialoffer|specialoffers)@'i   => 5.0],
    [qr'^(stockalert|stopsnoring|wantsome|workathome|yesitsfree)@'i  => 5.0],
    [qr'^(your_friend|greatoffers)@'i                                => 5.0],
    [qr'^(inkjetplanet|marketopt|MakeMoney)\d*@'i                    => 5.0],
   ),

   { # a hash-type lookup table (associative array)
     'nobody@cert.org'                        => -3.0,
     'cert-advisory@us-cert.gov'              => -3.0,
     'owner-alert@iss.net'                    => -3.0,
     'slashdot@slashdot.org'                  => -3.0,
     'securityfocus.com'                      => -3.0,
     'ntbugtraq@listserv.ntbugtraq.com'       => -3.0,
     'security-alerts@linuxsecurity.com'      => -3.0,
     'mailman-announce-admin@python.org'      => -3.0,
     'amavis-user-admin@lists.sourceforge.net'=> -3.0,
     'amavis-user-bounces@lists.sourceforge.net' => -3.0,
     'spamassassin.apache.org'                => -3.0,
     'notification-return@lists.sophos.com'   => -3.0,
     'owner-postfix-users@postfix.org'        => -3.0,
     'owner-postfix-announce@postfix.org'     => -3.0,
     'owner-sendmail-announce@lists.sendmail.org'   => -3.0,
     'sendmail-announce-request@lists.sendmail.org' => -3.0,
     'donotreply@sendmail.org'                => -3.0,
     'ca+envelope@sendmail.org'               => -3.0,
     'noreply@freshmeat.net'                  => -3.0,
     'owner-technews@postel.acm.org'          => -3.0,
     'ietf-123-owner@loki.ietf.org'           => -3.0,
     'cvs-commits-list-admin@gnome.org'       => -3.0,
     'rt-users-admin@lists.fsck.com'          => -3.0,
     'clp-request@comp.nus.edu.sg'            => -3.0,
     'surveys-errors@lists.nua.ie'            => -3.0,
     'emailnews@genomeweb.com'                => -5.0,
     'yahoo-dev-null@yahoo-inc.com'           => -3.0,
     'returns.groups.yahoo.com'               => -3.0,
     'clusternews@linuxnetworx.com'           => -3.0,
     lc('lvs-users-admin@LinuxVirtualServer.org')    => -3.0,
     lc('owner-textbreakingnews@CNNIMAIL12.CNN.COM') => -5.0,

     # soft-blacklisting (positive score)
     'sender@example.net'                     =>  3.0,
     '.example.net'                           =>  1.0,

   },
  ],  # end of site-wide tables
});


@decoders = (
  ['mail', \&do_mime_decode],
  ['F',    \&do_uncompress, ['unfreeze', 'freeze -d', 'melt', 'fcat'] ],
  ['Z',    \&do_uncompress, ['uncompress', 'gzip -d', 'zcat'] ],
  ['gz',   \&do_uncompress, 'gzip -d'],
  ['gz',   \&do_gunzip],
  ['bz2',  \&do_uncompress, 'bzip2 -d'],
  ['xz',   \&do_uncompress,
           ['xzdec', 'xz -dc', 'unxz -c', 'xzcat'] ],
  ['lzma', \&do_uncompress,
           ['lzmadec', 'xz -dc --format=lzma',
            'lzma -dc', 'unlzma -c', 'lzcat', 'lzmadec'] ],
  ['lrz',  \&do_uncompress,
           ['lrzip -q -k -d -o -', 'lrzcat -q -k'] ],
  ['lzo',  \&do_uncompress, 'lzop -d'],
  ['lz4',  \&do_uncompress, ['lz4c -d'] ],
  ['rpm',  \&do_uncompress, ['rpm2cpio.pl', 'rpm2cpio'] ],
  [['cpio','tar'], \&do_pax_cpio, ['pax', 'gcpio', 'cpio'] ],
  ['deb',  \&do_ar, 'ar'],
  ['rar',  \&do_unrar, ['unrar', 'rar'] ],
  ['arj',  \&do_unarj, ['unarj', 'arj'] ],
  ['arc',  \&do_arc,   ['nomarch', 'arc'] ],
  ['zoo',  \&do_zoo,   ['zoo', 'unzoo'] ],
  ['doc',  \&do_ole,   'ripole'],
  ['cab',  \&do_cabextract, 'cabextract'],
  ['tnef', \&do_tnef_ext, 'tnef'],
  ['tnef', \&do_tnef],
  [['zip','kmz'], \&do_7zip,  ['7za', '7z'] ],
  [['zip','kmz'], \&do_unzip],
  ['7z',   \&do_7zip,  ['7zr', '7za', '7z'] ],
  [[qw(gz bz2 Z tar)],
           \&do_7zip,  ['7za', '7z'] ],
  [[qw(xz lzma jar cpio arj rar swf lha iso cab deb rpm)],
           \&do_7zip,  '7z' ],
  ['exe',  \&do_executable, ['unrar','rar'], 'lha', ['unarj','arj'] ],
);


@av_scanners = (
# ### http://www.clamav.net/
['ClamAV-clamd',
   \&ask_daemon, ["*", "clamd:127.0.0.1:3310"],
   qr/\bOK$/m, qr/\bFOUND$/m,
   qr/^.*?: (?!Infected Archive)(.*) FOUND$/m ],
);

@av_scanners_backup = ();


1;  # insure a defined return value
