#!/usr/bin/env bash

set -Eeuo pipefail

# check open ports
SS_OUT=$(ss -ltnH)
PORTS=( "$@" )
for port in "${PORTS[@]}"
do
      (echo "$SS_OUT" | grep -q ":$port ") || exit 1
done

# check supervisor status
NUM_NOT_RUNNING=$(supervisorctl status | grep -cv "RUNNING\|EXITED\|STOPPED" || true)
[[ ${NUM_NOT_RUNNING} == 0 ]] || exit 1

# explicit exit code
exit 0
