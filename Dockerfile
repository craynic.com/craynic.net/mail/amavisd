FROM alpine:3

COPY files/ /

ENV AMAVISD_HOSTNAME="" \
    AMAVISD_CLAMAV_SERVERS=""

RUN apk add --no-cache \
        supervisor~=4 \
        inotify-tools~=3 \
        iproute2~=6 \
        bash~=5 \
        run-parts~=4 \
        amavis~=2 \
        amavisd-milter~=1 \
        spamassassin~=3 \
        altermime~=0 \
        p7zip~=22 \
        cabextract~=1 \
        rpm2cpio~=1 \
        lrzip~=0 \
        unarj~=2 \
        lha~=1 \
        stunnel~=5 \
        gettext~=0 \
    && apk upgrade --no-cache \
        # various CVEs fixed in 3.0.8
        libssl3 libcrypto3 \
    && mkdir -p /run/stunnel/ \
    && chmod -R o-rwx /var/amavis/ \
    && chown -R stunnel:stunnel /run/stunnel/

EXPOSE 10024 10026

VOLUME ["/var/lib/spamassassin", "/var/amavis/quarantine"]

HEALTHCHECK --interval=5s --timeout=5s --start-period=15s CMD "/usr/local/sbin/healthcheck.sh" 10024 10026

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]
CMD ["/usr/local/sbin/supervisord.sh"]
